﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ReteleTema4
{
    public class Client :INetworking
    {
        public Client()
        {
            Task.Run(() => Run());
        }

        public Message MessageSocket { get; set; }
        
        public string Message { get; set; }
        public void InitiateConnection(Host host,string messageString)
        {
            Message = messageString;
            Message message = new Message(0);
            this.SendSignedMessage(host,message);
        }

        private void Run()
        {
            while (true)
            {
                Thread.Sleep(200);
                if(MessageSocket==null)continue;
                if(MessageSocket.F==0)continue;
                int x = MessageSocket.X + MessageSocket.F - 1;
                string messagePart = Message.Substring(0,Math.Min(MessageSocket.F,Message.Length));
                Message message;
                if (string.IsNullOrEmpty(messagePart)&& MessageSocket.F>0)
                {
                    message = new Message(0) {Fin = 1};
                }
                else
                {
                    Message = Message.Substring(messagePart.Length, Message.Length - messagePart.Length);
                    message = new Message(messagePart, x, MessageSocket.F);
                }
               this.SendSignedMessage(MessageSocket.Sender, message);
                MessageSocket = null;
            }
        }

    }
}