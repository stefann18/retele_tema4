﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ReteleTema4
{
    public class Host :INetworking
    {
        public Host()
        {
            Task =Task.Run(() => Run());
        }

        public Message MessageSocket { get; set; }

        public string ReceivedMessage { get; set; }
        public Task Task { get; set; }

        private void Run()
        {
            Random random = new Random();
            while (true)
            {
                Thread.Sleep(200);
                if (MessageSocket == null) continue;

                ReceivedMessage += MessageSocket.MessageBytes;
                Console.WriteLine(ReceivedMessage);
                if (MessageSocket.Fin == 1)
                {
                    Console.WriteLine(ReceivedMessage);
                    break;

                }
                int x = MessageSocket.X+1;
                int f = random.Next(0, 3);
                Message message = new Message(x,f);
                
                this.SendSignedMessage(MessageSocket.Sender,message);

                if (f != 0)
                {
                    MessageSocket = null;

                }
                else
                {
                    MessageSocket.MessageBytes = "";
                }
            }
        }

    }
}