﻿namespace ReteleTema4
{
    public class Message
    {
        public Message(int x)
        {
            X = x;
            F = 0;
            ACK = 1;
            SYN = 0;
            Fin = 0;
        }

        public Message(int x, int f)
        {
            X = x;
            F = f;
            ACK = 1;
            SYN = 1;
            Fin = 0;
        }
        public Message(string messageBytes,int x, int f)
        {
            MessageBytes = messageBytes;
            X = x;
            F = f;
            ACK = 1;
            SYN = 1;
            Fin = 0;
        }



        public string MessageBytes { get; set; }
        public int X { get; set; }
        public int ACK { get; set; }
        public int SYN { get; set; }
        public int F { get; set; }
        public int Fin { get; set; }
        public INetworking Sender { get; set; }
    }

    public static class MessageUtil {
        public static void SendSignedMessage(this INetworking iNetworkingSender,INetworking iNetworkingTarget,Message message)
        {
            message.Sender = iNetworkingSender;
            iNetworkingTarget.MessageSocket = message;
        }
    }
}