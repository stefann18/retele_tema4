﻿using System;
using System.Threading.Tasks;

namespace ReteleTema4
{
    class Program
    {
        static void Main(string[] args)
        {
            Client client = new Client();
            Host host = new Host();
            Task t = host.Task;
            client.InitiateConnection(host,"Reflective mirrors are awesome!");

            t.Wait();
        }
    }
}
